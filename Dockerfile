FROM jupyter/tensorflow-notebook

# Looks like we need root user to install swig
USER root
RUN apt-get update && apt-get install -y \
  swig \
  curl

# Need to install as user jovyan. Error otherwise
USER jovyan

# auto-sklean dependencies
RUN curl https://raw.githubusercontent.com/automl/auto-sklearn/master/requirements.txt | xargs -n 1 -L 1 pip install

RUN pip install auto-sklearn

