### Description

A simple example usage of the docker image [steelersd/jupyter-auto-sklearn](https://cloud.docker.com/repository/docker/steelersd/jupyter-auto-sklearn)

Run docker compose  
`docker-compose up`

With custom port  
`JUPYTER_PORT=9001 docker-compose up`

Run from command line  
`docker run -it -p 8888:8888 -v $PWD/:/home/jovyan/work steelersd/jupyter-auto-sklearn:1.0`

To build  
`docker-compose build`

Can also run using run-docker.sh  
`./run-docker.sh`

### Helpful docker commands

List all containers (only IDs) `docker ps -aq`  
Stop all running containers. `docker stop $(docker ps -aq)`  
Remove all containers. `docker rm $(docker ps -aq)`  
Remove all images. `docker rmi $(docker images -q)`

Get security token . `docker exec 4bdd0e4841e0 jupyter notebook list`
